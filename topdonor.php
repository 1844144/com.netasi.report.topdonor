<?php

require_once 'topdonor.civix.php';

/**
 * Implementation of hook_civicrm_config
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function topdonor_civicrm_config(&$config) {
  _topdonor_civix_civicrm_config($config);
}

/**
 * Implementation of hook_civicrm_xmlMenu
 *
 * @param $files array(string)
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function topdonor_civicrm_xmlMenu(&$files) {
  _topdonor_civix_civicrm_xmlMenu($files);
}

/**
 * Implementation of hook_civicrm_install
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function topdonor_civicrm_install() {
  _topdonor_civix_civicrm_install();
}

/**
 * Implementation of hook_civicrm_uninstall
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function topdonor_civicrm_uninstall() {
  _topdonor_civix_civicrm_uninstall();
}

/**
 * Implementation of hook_civicrm_enable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function topdonor_civicrm_enable() {
  _topdonor_civix_civicrm_enable();
}

/**
 * Implementation of hook_civicrm_disable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function topdonor_civicrm_disable() {
  _topdonor_civix_civicrm_disable();
}

/**
 * Implementation of hook_civicrm_upgrade
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed  based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function topdonor_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _topdonor_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implementation of hook_civicrm_managed
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function topdonor_civicrm_managed(&$entities) {
  _topdonor_civix_civicrm_managed($entities);
}

/**
 * Implementation of hook_civicrm_caseTypes
 *
 * Generate a list of case-types
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function topdonor_civicrm_caseTypes(&$caseTypes) {
  _topdonor_civix_civicrm_caseTypes($caseTypes);
}

function topdonor_civicrm_alterReportVar($varType, &$var, &$object){
  $name = $object->getVar('_name');
  if (!empty($name) && ($name=='TopDonor') ){

    if ($varType == 'columns') {
      $var['civicrm_lastdate']['fields']['last_donation_date'] = array (
        'title' => ts('Date of last donation'),
        'type' => CRM_Report_Form::OP_STRING,
      );
      $var['civicrm_lastdate']['fields']['last_total_amount'] = array (
        'title' => ts('Amount of last donation'),
        'type' => CRM_Report_Form::OP_INT,
      );
    } 

    if ($varType == 'sql'){
      $select = $var->getVar('_select');
      if ( strpos($select,'lastdate') ) {
        $from = $var->getVar('_from');
        $from .= " LEFT JOIN 
          ( 
            SELECT  
              con1.contact_id, 
              DATE_FORMAT(con1.receive_date,'%d %b %Y') as last_donation_date, 
              con1.total_amount as last_total_amount 
            from civicrm_contribution con1 
            LEFT OUTER JOIN civicrm_contribution con2 on 
            (con1.contact_id=con2.contact_id and con1.receive_date<con2.receive_date)
            where con2.id is NULL 
          ) lastdate_civireport 
          on lastdate_civireport.contact_id=contact_civireport.id ";
        $var->setVar('_from',$from);
      }
    }   
  }
}

/**
 * Implementation of hook_civicrm_alterSettingsFolders
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function topdonor_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _topdonor_civix_civicrm_alterSettingsFolders($metaDataFolders);
}
